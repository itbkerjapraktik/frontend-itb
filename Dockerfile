FROM node:8.11-alpine

LABEL maintainer="Publc"

WORKDIR /var/www/html

COPY . .

RUN apk update \
  apk add --update python python-dev py-pip build-base \
  && apk add git zip unzip vim nano \
  && npm install \ 
  && rm -rf /var/cache/apk/* \
  && apk add --no-cache bash

ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD ["npm", "run", "build"]