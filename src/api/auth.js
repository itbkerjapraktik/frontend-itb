import axios from 'axios'

// const api_url = Auth.api_url+'auth/admin'
const api_url = 'http://103.234.209.42:8090/'
// const api_url_img = 'http://api.itbfitnessindonesia.com/'

const login = function (data) {
  return axios.post(api_url+'session/set/1', data).then(res => { return res.data })
}

const Session = {
  delete: function (item) {
    if (!item) {
      localStorage.removeItem('itb_token')
      // localStorage.removeItem('itb_id')
      // localStorage.removeItem('itb_username')
    } else {
      localStorage.removeItem(item)
    }
  },
  create: function (session) {
    // localStorage.setItem('itb_id', session.admin_id)
    localStorage.setItem('itb_token', session.token)
    // localStorage.setItem('itb_username', session.username)
  },
  get: function (item) {
    if (!item) {
      let session = {
        // itb_id: localStorage.getItem('itb_id'),
        itb_token: localStorage.getItem('itb_token'),
        // itb_username: localStorage.getItem('itb_username')
      }
      return session
    } else {
      return localStorage.getItem(item)
    }
  }
  // getAuth () {
  //   return {
  //     elapor_token: localStorage.getItem('elapor_token')
  //   }
  // }
}

export {
  Session,
  login,
  api_url
  // api_url_img
}
