import Vue from 'vue'
import Router from 'vue-router'
import { Auth } from '@/api'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

const Colors = () => import('@/views/theme/Colors')
const Typography = () => import('@/views/theme/Typography')

const Charts = () => import('@/views/Charts')
const Widgets = () => import('@/views/Widgets')

// Views - Components
const Cards = () => import('@/views/base/Cards')
const Forms = () => import('@/views/base/Forms')
const Switches = () => import('@/views/base/Switches')
const Tables = () => import('@/views/base/Tables')
const Tabs = () => import('@/views/base/Tabs')
const Breadcrumbs = () => import('@/views/base/Breadcrumbs')
const Carousels = () => import('@/views/base/Carousels')
const Collapses = () => import('@/views/base/Collapses')
const Jumbotrons = () => import('@/views/base/Jumbotrons')
const ListGroups = () => import('@/views/base/ListGroups')
const Navs = () => import('@/views/base/Navs')
const Navbars = () => import('@/views/base/Navbars')
const Paginations = () => import('@/views/base/Paginations')
const Popovers = () => import('@/views/base/Popovers')
const ProgressBars = () => import('@/views/base/ProgressBars')
const Tooltips = () => import('@/views/base/Tooltips')

// Views - Buttons
const StandardButtons = () => import('@/views/buttons/StandardButtons')
const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
const Dropdowns = () => import('@/views/buttons/Dropdowns')
const BrandButtons = () => import('@/views/buttons/BrandButtons')

// Views - Icons
const Flags = () => import('@/views/icons/Flags')
const FontAwesome = () => import('@/views/icons/FontAwesome')
const SimpleLineIcons = () => import('@/views/icons/SimpleLineIcons')
const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')

// Views - Notifications
const Alerts = () => import('@/views/notifications/Alerts')
const Badges = () => import('@/views/notifications/Badges')
const Modals = () => import('@/views/notifications/Modals')

// Views - Auth
const Login = () => import('@/views/auth/Login')
const Register = () => import('@/views/auth/Register')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')

const PengajuanKp = () => import('@/views/pages/pengajuanKp/index')
const PengajuanKpUser = () => import('@/views/pages/pengajuanKpUser/index')

const ListPerusahaan = () => import('@/views/pages/listPerusahaan/index')
const AddListPerusahaan = () => import('@/views/pages/listPerusahaan/add')
const EditListPerusahaan = () => import('@/views/pages/listPerusahaan/edit')

const Lowongan = () => import('@/views/pages/lowongan/index')
const AddLowongan = () => import('@/views/pages/lowongan/add')
const EditLowongan = () => import('@/views/pages/lowongan/edit')

const Coaching = () => import('@/views/pages/coaching/index')
const AddCoaching = () => import('@/views/pages/coaching/add')
const EditCoaching = () => import('@/views/pages/coaching/edit')

const ListPenggunaMahasiswa = () => import('@/views/pages/listPengguna/mahasiswa')
const ListPenggunaTu = () => import('@/views/pages/listPengguna/tu')
const ListPenggunaDosen = () => import('@/views/pages/listPengguna/dosen')

const FormPenilaian = () => import('@/views/pages/penilaian/form')
const ListPenilaian = () => import('@/views/pages/penilaian/index')

const Laporan = () => import('@/views/pages/laporan/index')
const LaporanMahasiswa = () => import('@/views/pages/laporan/mahasiswa')
const AddTitleLaporan = () => import('@/views/pages/laporan/titleAdd')
const EditTitleLaporan = () => import('@/views/pages/laporan/titleEdit')

Vue.use(Router)

const router = new Router({
  // mode: 'hash', // https://router.vuejs.org/api/#mode
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        }
      ]
    },
    {
      path: '/',
      redirect: '/dashboard',
      // name: 'CMS',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          meta: { requiresAuth: true },
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'pengajuanKp',
          name: 'Pengajuan KP',
          meta: { requiresAuth: true },
          component: PengajuanKp
        },
        {
          path: 'pengajuanKpUser',
          name: 'Pengajuan KP User',
          meta: { requiresAuth: true },
          component: PengajuanKpUser
        },
        {
          path: 'listPerusahaan',
          name: 'List Perusahaan',
          meta: { requiresAuth: true },
          component: ListPerusahaan
        },
        {
          path: 'listPerusahaan/add',
          name: 'Add List Perusahaan',
          meta: { requiresAuth: true },
          component: AddListPerusahaan
        },
        {
          path: 'listPerusahaan/edit',
          name: 'Edit List Perusahaan',
          meta: { requiresAuth: true },
          component: EditListPerusahaan
        },
        {
          path: 'coaching',
          name: 'List Coaching',
          meta: { requiresAuth: true },
          component: Coaching
        },
        {
          path: 'coaching/add',
          name: 'Add Coaching',
          meta: { requiresAuth: true },
          component: AddCoaching
        },
        {
          path: 'coaching/edit',
          name: 'Edit Coaching',
          meta: { requiresAuth: true },
          component: EditCoaching
        },
        {
          path: 'lowongan',
          name: 'List Lowongan',
          meta: { requiresAuth: true },
          component: Lowongan
        },
        {
          path: 'lowongan/add',
          name: 'Add Lowongan',
          meta: { requiresAuth: true },
          component: AddLowongan
        },
        {
          path: 'lowongan/edit',
          name: 'Edit Lowongan',
          meta: { requiresAuth: true },
          component: EditLowongan
        },
        {
          path: 'mahasiswa',
          name: 'List Mahasiswa',
          meta: { requiresAuth: true },
          component: ListPenggunaMahasiswa
        },
        {
          path: 'tu',
          name: 'List TU',
          meta: { requiresAuth: true },
          component: ListPenggunaTu
        },
        {
          path: 'dosen',
          name: 'List Dosen',
          meta: { requiresAuth: true },
          component: ListPenggunaDosen
        },
        {
          path: 'penilaian/form',
          name: 'Form Penilaian',
          meta: { requiresAuth: true },
          component: FormPenilaian
        },
        {
          path: 'penilaian',
          name: 'List Penilaian',
          meta: { requiresAuth: true },
          component: ListPenilaian
        },
        {
          path: 'laporan',
          name: 'Laporan',
          meta: { requiresAuth: true },
          component: Laporan
        },
        {
          path: 'laporan/titleAdd',
          name: 'Tambah Judul Laporan',
          meta: { requiresAuth: true },
          component: AddTitleLaporan
        },
        {
          path: 'laporan/titleEdit',
          name: 'Ubah Judul Laporan',
          meta: { requiresAuth: true },
          component: EditTitleLaporan
        },
        {
          path: 'laporanMahasiswa',
          name: 'Laporan Mahasiswa',
          meta: { requiresAuth: true },
          component: LaporanMahasiswa
        }
      ]
    }
  ]
})
// $route.query redirect di login biar lgsung masuk ke spesifik
router.beforeEach((to, from, next) => {
  let session = Auth.Session.get()
  if (to.meta.requiresAuth) {
    if (!session.itb_token) {
      Auth.Session.delete()
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    } else next()
  } else next()
})

export default router