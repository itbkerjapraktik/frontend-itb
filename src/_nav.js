export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home'
    },
    {
      name: 'Pengajuan KP',
      url: '/pengajuanKp',
      icon: 'icon-docs'
    },
    {
      name: 'Pengajuan KP User',
      url: '/pengajuanKpUser',
      icon: 'icon-note'
    },
    // {
    //   name: 'Coaching',
    //   url: '/coaching',
    //   icon: 'icon-briefcase'
    // },
    {
      name: 'Lowongan',
      url: '/lowongan',
      icon: 'icon-pie-chart'
    },
    {
      name: 'List Perusahaan',
      url: '/listPerusahaan',
      icon: 'icon-briefcase'
    },
    {
      name: 'Laporan',
      url: '/laporan',
      icon: 'icon-notebook'
    },
    {
      name: 'Laporan Mahasiswa',
      url: '/laporanMahasiswa',
      icon: 'icon-notebook'
    },
    {
      name: 'Penilaian',
      url: '/penilaian',
      icon: 'icon-notebook'
    },
    {
      name: 'List Pengguna',
      url: '/listPengguna',
      icon: 'icon-people',
      children: [
        {
          name: 'Mahasiswa',
          url: '/mahasiswa',
          icon: 'icon-user'
        },
        {
          name: 'TU',
          url: '/tu',
          icon: 'icon-user'
        },
        {
          name: 'Dosen',
          url: '/dosen',
          icon: 'icon-user'
        }
      ]
    }
  ]
}
